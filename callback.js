function fakeAjax(url, cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function () {
		cb(fake_responses[url]);
	}, randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************
// The old-n-busted callback way

function getFile(file) {
	fakeAjax(file, function (text) {
		filetest(file, text);

		//console.log(text);

	});
}
getFile("file1");
getFile("file2");
getFile("file3");

var obj = {};
var getfile_arr = ["file1", "file2", "file3"];
// request all files at once in "parallel"
function filetest(file, text) {
	if (!obj[file]) {
		obj[file] = text;
	}
	for (let i = 0; i < getfile_arr.length; i++) {
		if (obj.hasOwnProperty(getfile_arr[i])) {
			if (obj[getfile_arr[i]] != true) {
				output(obj[getfile_arr[i]]);
				obj[getfile_arr[i]] = true;
			}
		}
		else {
			return false;
		}
	}
}




