function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************
// The old-n-busted callback way

function getFile(file) {
    return new Promise((resovle,request)=>{
	fakeAjax(file,function(text){
		// what do we do here?
        resovle(text);
    });
	});
}

// request all files at once in "parallel"
file1=getFile("file1");
file2=getFile("file2");
file3=getFile("file3");
file1.then((data)=>
{
    console.log(data);
    return file2;
}).then((data)=>{
    console.log(data);
    return file3;
}).then((data)=>{
    console.log(data);
})
//if we want to return all of them after finishing we use Promise.all
//if we want anyone one of them is finished we use Promise.racefile1.then((data)=>
