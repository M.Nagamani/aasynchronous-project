function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);
 
	setTimeout(function(){
		cb(fake_responses[url]);
	},randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************
// The old-n-busted callback way

function getFile(file) {
	return new Promise((resolve,reject)=>{
	fakeAjax(file,function(text){
     resolve(text);

//console.log(text);
	});
	});
}
file1=getFile("file1");
file2=getFile("file2");
file3=getFile("file3");
async function opening()
{
  const a1=await file1;
  console.log(a1);
  const a2=await file2;
  console.log(a2);
  const a3=await file3;
  console.log(a3);
  }
  opening();
  
